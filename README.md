# QUA Puppet ohmyzsh

Puppet module for installing and configuring ohmyzsh. Is a branch of [an existing ohmyzsh-puppet module](https://github.com/acme/puppet-acme-oh-my-zsh)

The Quango branch adds: 

* Bugfix: https://bitbucket.org/quango/qua-puppet-ohmyzsh/commits/f7f69f8783cce31be9e43a5324dfbe8d619f5e8b
* Pull oh-my-zsh from robbyrussell via https:// rather than git:// (firewall compatibility)